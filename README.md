# Latber Singaraja: Uniting Arrows, Uniting Hearts
![Header](images/header_latber.jpg)


## 1. Deskripsi Acara

**Nama Acara:** Latber Singaraja

**Tema:** *Uniting Arrows, Uniting Hearts* – Latihan Bersama Sederhana yang mengusung semangat persatuan dan kebersamaan para pemanah tradisional di Bali.

**Lokasi:** Pusat Latihan Madani HBA Club

**Waktu:** Ahad, 14 Juli 2024

**Tujuan:** Menghidupkan kembali semangat panahan tradisional dan memperkuat persatuan di antara pemanah tradisional di Bali. Acara ini juga bertujuan untuk memotivasi pemanah agar terus berlatih dengan semangat baru.

***Acara ini TERBUKA untuk para pemanah tradisional dari seluruh organisasi panahan tradisional di Bali***

## 2. Teknis Permainan

**Format Permainan:**

| Babak              | Jumlah Anak Panah | Jumlah Rambahan | Keterangan              |
|--------------------|-------------------|-----------------|--------------------------|
| Babak Kualifikasi  | 7                 | 8               | Akumulasi poin terbaik   |
| Babak 16 Besar     | 7                 | 2               | Akumulasi poin terbaik   |
| Babak 8 Besar      | 7                 | 2               | Akumulasi poin terbaik   |
| Semi Final         | 7                 | 2               | Sistem gugur             |
| Perebutan Juara 3  | 7                 | 2               | Sistem gugur             |
| Final              | 7                 | 2               | Sistem gugur             |

**Peraturan Utama:**
- Menggunakan busur tradisional, busur horsebow, atau busur Asiatik.
- Anak panah harus terbuat dari kayu atau bambu dengan vanes natural.
- Menggunakan selfnock atau nock natural, bukan plastik.
- Menggunakan quiver; tidak diperkenankan memegang lebih dari satu anak panah saat shot.
- Teknik memanah bebas.
- Peserta dipersilahkan mengenakan jersey panahan, pakaian tradisional atau menambahkan aksesoris yang mencerminkan budaya Indonesia.
- Bersikap Sopan dan Bertegur sapa dengan warga sekitar.

**Kategori:**
- **Putra:** Jarak 50 meter, usia minimal 13 Tahun
- **Putri:** Jarak 30 meter, usia minimal 13 Tahun

**Penilaian:**
- Menggunakan target yang telah ditentukan.
  - Ring luar: **1 poin**, 
  - Ring tengah: **2 poin**.
- Babak Playoff:
  - Babak ini dilakukan hanya jika terjadi nilai yang sama sehingga perlu dilakukan untuk menentukan siapa yang menang atau lanjut ke babak selanjutnya. 
  - Shoot-off dengan 1 anak panah, di mana poin terbanyak atau yang paling dekat dengan titik tengah menjadi pemenang.
 
**Peralatan yang Digunakan:**
- Busur tradisional, anak panah kayu atau bambu dengan vanes natural, selfnock atau nock natural, dan quiver.

**Target yang Digunakan:**

![Target](face_target_latber_singaraja.png)

- **Ring Luar:** 60 cm
- **Ring Tengah:** 30 cm

*Cetak file ini dengan ukuran 80x80 cm, maka otomatis diameter luar (warna maroon) akan menjadi 60 cm.*

Baik kategori Putra dan Putri, **menggunakan diameter target yang sama**.

[Download Face Target](https://gitlab.com/lubislutfi/latber_madani_handbook/-/blob/main/face_target_latber_singaraja.png)


## 3. Jadwal Acara

Jadwal acara pada 14 Juli 2024:

| Waktu             | Acara                                         |
|-------------------|-----------------------------------------------|
| 07.30 - 08.30     | Registrasi dan test shoot lapangan            |
| 08.30 - 08.50     | Pembukaan dan foto bersama                    |
| 08.50 - 09.00     | Persiapan Kategori Putri                      |
| 09.00 - 10.15     | Babak Kualifikasi Putri                       |
| 10.30 - 10.40     | Persiapan Kategori Putra                      |
| 10.40 - 12.25     | Babak Kualifikasi Putra                       |
| **12.25 - 13.20** | **Istirahat dan persiapan babak selanjutnya** |
| 13.20 - 14.05     | Babak 16 Besar (Putra & Putri)                |
| 14.05 - 14.35     | Babak 8 Besar (Putra & Putri)                 |
| 14.35 - 15.00     | Babak Semi Final (Putra & Putri)              |
| 15.00 - 15.20     | Babak Perebutan Juara 3 (Putra & Putri)       |
| 15.20 - 15.45     | Babak Final dan penutupan                     |

## 4. Pendaftaran

**Cara Mendaftar:**
Isi formulir pendaftaran di [bit.ly/latbersingaraja](https://bit.ly/latbersingaraja)

**Biaya Pendaftaran:**
Rp. 50.000

**Pembayaran:**
Transfer Bank BCA 8271298609 atas nama Suraida, konfirmasi pembayaran ke Bu Eda [(081936422676)](https://wa.me/6281936422676).

**Batas Waktu Pendaftaran:**
Kamis, 11 Juli 2024

**List Peserta:**
Peserta dapat mengecek status pendaftaran dan daftar peserta di [bit.ly/pesertalatbersingaraja](https://bit.ly/pesertalatbersingaraja)


## 5. Bebungah (Donasi Hadiah)
Bebungah bagian penting dari acara ini. Bebungah tidak hanya menambah semarak acara, tetapi juga menjadi bentuk apresiasi dan dukungan Anda terhadap komunitas pemanah tradisional di Bali. 

Dengan donasi Anda, kita bisa membuat acara ini menjadi lebih berkesan dan bermakna bagi semua peserta.  Mari kita semarakkan Latber Singaraja dengan Bebungah Anda!

Jika Anda berminat untuk berpartisipasi dalam Bebungah, <br>Silakan hubungi **Kang Rahman** [(081246256363)](https://wa.me/6281246256363).

## 5. Contact Person
### - **Kategori Putri:** Bu Eda [(081936422676)](https://wa.me/6281936422676)
### - **Kategori Putra:** Pak Budi [(08179713286)](https://wa.me/628179713286)

---

Diharapkan handbook ini dapat memberikan panduan yang jelas dan rinci bagi semua peserta dan panitia.

Semoga acara Latber Singaraja ini berjalan sukses dan membawa manfaat bagi semua pemanah di Bali.

Jika ada pertanyaan, silahkan hubungi contact person yang tertera di atas.

---

**TEKNIS LOMBA DAN JADWAL YANG TERTERA DI ATAS DAPAT BERUBAH SEWAKTU-WAKTU MENYESUAIKAN SITUASI DAN KONDISI YANG ADA.**






